package co.edu.unimagdalena.apmoviles.universidad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class EstudianteController {

    private static BaseDatos bd;
    private static Context c;

    public EstudianteController( Context c) {
        this.bd = new BaseDatos(c,1);
        this.c = c;
    }

    public EstudianteController(BaseDatos bd, Context c) {
        this.bd = bd;
        this.c = c;
    }

    public static void agregarEstudiante(Estudiante e) {
        try {
            SQLiteDatabase sql = bd.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DefBD.col_codigo, e.getCodigo());
            values.put(DefBD.col_nombre, e.getNombre());
            values.put(DefBD.col_programa, e.getPrograma());
            long id = sql.insert(DefBD.tabla_est, null, values);

            Toast.makeText(c, "Estudiante registrado", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(c, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
    public boolean editEstudiante(Estudiante e) {
        String[] args = new String []{e.getCodigo()};
        SQLiteDatabase sql = bd.getReadableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("codigo", e.getCodigo());
        cv.put("nombre", e.getNombre());
        cv.put("programa", e.getPrograma());

        try {
            sql.update(DefBD.tabla_est, cv, "codigo=?", args);
            return true;
        } catch (Exception ex){
            Toast.makeText(c, "Error editando estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return false;
        }

    }
    public boolean deleteEstudiante(String cod) {
        String args[] = new String[] {cod};
        SQLiteDatabase sql = bd.getReadableDatabase();
        return sql.delete(DefBD.tabla_est, "codigo=?", args) > 0;
    }
    public static boolean buscarEstudiante(Estudiante e) {
        String args[] = new String[] {e.getCodigo()};
        String col[] = new String[] {DefBD.col_codigo, DefBD.col_nombre};
        SQLiteDatabase sql = bd.getReadableDatabase();
        Cursor c = sql.query(DefBD.tabla_est, null, "codigo=?", args, null, null, null);
        if (c.getCount() > 0) {
            bd.close();
            return true;
        } else {
            bd.close();
            return false;
        }
    }

    public Estudiante getEstudiante(String cod) {
        String args[] = new String[] {cod};
        String col[] = new String[] {DefBD.col_codigo, DefBD.col_nombre};
        SQLiteDatabase sql = bd.getReadableDatabase();
        Cursor cursor = sql.query(DefBD.tabla_est, null, "codigo=?", args, null, null, null);

        // To increase performance first get the index of each column in the cursor
        final int codIndex = cursor.getColumnIndex("codigo");
        final int nameIndex = cursor.getColumnIndex("nombre");
        final int programIndex = cursor.getColumnIndex("programa");

        try {

            // If moveToFirst() returns false then cursor is empty
            if (!cursor.moveToFirst()) {
                //return new ArrayList<>();
                return null;
            }

            final List<Estudiante> products = new ArrayList<>();

            do {

                // Read the values of a row in the table using the indexes acquired above
                final String name = cursor.getString(nameIndex);
                final String code = cursor.getString(codIndex);
                final String program = cursor.getString(programIndex);

                products.add(new Estudiante(code, name, program));

            } while (cursor.moveToNext());

            return products.get(0);

        }
        catch (Exception ex){
            Toast.makeText(c, "Error consulta estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
        finally {
            // Don't forget to close the Cursor once you are done to avoid memory leaks.
            // Using a try/finally like in this example is usually the best way to handle this
            cursor.close();

            // close the database
            sql.close();
        }

    }

    public Cursor allEstudiantes(){
        try{
            SQLiteDatabase sql = bd.getReadableDatabase();
            Cursor c = sql.query(DefBD.tabla_est,null,null,null,null,null,null);
            return c;
        }
        catch (Exception ex){
            Toast.makeText(c, "Error consulta estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public Cursor allEstudiantes2(){
        try{
            SQLiteDatabase sql = bd.getReadableDatabase();
            Cursor cur = sql.rawQuery("select codigo as _id , nombre, programa from estudiante", null);
            return cur;
        }
        catch (Exception ex){
            Toast.makeText(c, "Error consulta estudiantes " + ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }

}
