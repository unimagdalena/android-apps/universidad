package co.edu.unimagdalena.apmoviles.universidad;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Estudiante e;
    private EditText codigo, nombre, programa;
    private Button agregar, cancelar, mostrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        codigo = findViewById(R.id.edtcodigo);
        nombre = findViewById(R.id.edtnombre);
        programa = findViewById(R.id.edtprograma);

        agregar = findViewById(R.id.btnguardar);
        cancelar = findViewById(R.id.btncancelar);
        mostrar = findViewById(R.id.btnlistado);

        agregar.setOnClickListener(this);
        cancelar.setOnClickListener(this);
        mostrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        String cod = codigo.getText().toString();
        String name = nombre.getText().toString();
        String program = programa.getText().toString();
        switch (view.getId()) {
            case R.id.btnguardar:
                if (TextUtils.isEmpty(cod) || TextUtils.isEmpty(name) || TextUtils.isEmpty(program)) {
                    Toast.makeText(this, "los datos no pueden ser vacios", Toast.LENGTH_LONG).show();
                    break;
                }
                e = new Estudiante(cod, name, program);
                if (EstudianteController.buscarEstudiante(e)) {
                    Toast.makeText(this, "El estudiante ya está registrado", Toast.LENGTH_LONG).show();
                    break;
                }
                EstudianteController.agregarEstudiante(e);
                break;
            case R.id.btncancelar:

                break;
            case R.id.btnlistado:
                Intent i = new Intent(this, ListadoActivity.class);
                startActivity(i);
                break;
        }

    }
}
