package co.edu.unimagdalena.apmoviles.universidad;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EstudianteActivity extends AppCompatActivity {

    private EditText codigo, nombre, programa;
    private Button agregar;
    EstudianteController estudianteController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiante);
        estudianteController = new EstudianteController(this);
        Bundle extras = getIntent().getExtras();

        codigo = findViewById(R.id.edtcod);
        nombre = findViewById(R.id.edtname);
        programa = findViewById(R.id.edtprogram);
        if (extras != null) {
            String cod = extras.getString("codigo");
            Estudiante est = estudianteController.getEstudiante(cod);
            codigo.setText(est.getCodigo());
            nombre.setText(est.getNombre());
            programa.setText(est.getPrograma());
        }

        agregar = findViewById(R.id.btnsave);
        agregar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Estudiante e = new Estudiante(codigo.getText().toString(), nombre.getText().toString(), programa.getText().toString());
                if (estudianteController.editEstudiante(e)) {
                    Toast.makeText(getApplicationContext(), "Estudiante editado correctamente", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(EstudianteActivity.this, ListadoActivity.class);
                    startActivity(intent);
                }
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (estudianteController.deleteEstudiante(getIntent().getExtras().getString("codigo"))) {
                    Snackbar.make(view, "Estudiante eliminado", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    Intent intent = new Intent(EstudianteActivity.this, ListadoActivity.class);
                    startActivity(intent);
                } else {
                    Snackbar.make(view, "ha ocurrido un error", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                //finish();
            }
        });
    }
}